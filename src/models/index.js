const User = require('./User');
const Exercise = require('./Exercise');
const Result = require('./Result');
const Token = require('./Token');

module.exports = {
    User,
    Exercise,
    Result,
    Token
};