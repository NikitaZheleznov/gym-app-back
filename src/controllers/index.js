const auth = require('./auth.controller');
const exercise = require('./exercise.controller');
const result = require('./result.controller')

module.exports = {
    auth,
    exercise,
    result
}