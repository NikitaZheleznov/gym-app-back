const {exercise} = require("../controllers");
module.exports = {
    routes: [
        'auth',
        'exercises',
        'results'
    ]
}